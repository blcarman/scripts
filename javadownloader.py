#! /usr/bin/env python
# script to download the latest version of oracle jdk

import requests
import re
import argparse


def download_java():
    java_major_version = "8"
    java_url = "http://www.oracle.com/technetwork/java/javase/downloads/index.html"
    java_page = requests.get(java_url)

    latest_version = max(re.findall(java_major_version + r'u(\d\d\d)',
                         java_page.text))
    java_long_version = java_major_version + "u" + latest_version
    download_url = "http://download.oracle.com/otn-pub/java/jdk/" + java_long_version + \
                   "-b11/a58eab1ec242421181065cdc37240b08/jdk-" + java_long_version + "-linux-x64.tar.gz"

    java_tarball = requests.get(download_url, cookies={'gpw_e24': 'http%3A%2F%2Fwww.oracle.com%2F',
                                                       'oraclelicense': 'accept-securebackup-cookie'})

    java_file = open(args.path + "/jdk-" + java_long_version + "-linux-x64.tar.gz", 'wb')
    for chunk in java_tarball.iter_content(100000):
        java_file.write(chunk)
    java_file.close()


def download_tomcat():
    tomcat_major_version = "7"
    tomcat_main_url = "http://mirror.metrocast.net/apache/tomcat/tomcat-" + tomcat_major_version
    tomcat_latest_version = re.search(tomcat_major_version + r'\d\.\d\.\d\d', requests.get(tomcat_main_url).text).group[0]
    tomcat_url = "http://mirror.metrocast.net/apache/tomcat/tomcat-" + tomcat_major_version + "/v" + \
                 tomcat_latest_version + "/bin/apache-tomcat-" + tomcat_latest_version + ".tar.gz"
    tomcat_tarball = requests.get(tomcat_url)
    tomcat_file = open(args.path + "/tomcat-" + tomcat_latest_version + ".tar.gz", "wb")
    for chunk in tomcat_tarball.iter_content(100000):
        tomcat_file.write(chunk)

    tomcat_file.close()


# def keep_ten():
if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("--java", help="download java", action="store_true")
    parser.add_argument("--tomcat", help="download tomcat", action="store_true")
    parser.add_argument("--path", help="download path", type=str, required=True)
    args = parser.parse_args()
    print(args.path)
    if args.java:
        print("downloading java")
        download_java()
    if args.tomcat:
        print("download tomcat")
        download_tomcat()
