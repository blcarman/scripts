# mediawiki to bookstack converter

import requests
import bs4
import time
from selenium import webdriver

wikiFQDN = "http://wiki.example.com"
bookFQDN = "http://book.example.com"

allPages = bs4.BeautifulSoup(requests.get(wikiFQDN + '/index.php/Special:AllPages').text, "html.parser")
bsAllPages = allPages.select('#mw-content-text')

browser = webdriver.Firefox()
browser.get(bookFQDN + '/login')

username = browser.find_element_by_id('email')
username.send_keys('notmyusername')

password = browser.find_element_by_id('password')
password.send_keys('notmypassword')
password.submit()
time.sleep(2)

for bullet in bsAllPages[0].descendants:
    if bullet.name == "a":
        # check if bookstack page exists, need to remove /wiki/index.php/
        checkURL = bookFQDN + "/books/lab/page" + \
            bullet['href'].replace('/wiki/index.php', '').replace('_', '-')
        if "Sorry" in requests.get(checkURL).text:
            print("creating new page")
            bsWikiPage = bs4.BeautifulSoup(requests.get(wikiFQDN +
                                           bullet['href']).text, "html.parser")
            bsContent = bsWikiPage.select('#mw-content-text')
            mdContent = ""

            for child in bsContent[0].children:
                if isinstance(child, bs4.element.Tag):
                    if "id" not in child.attrs:
                        if child.name == "p":
                            mdContent += child.get_text()
                            mdContent += "\n"
                        if child.name.startswith("h"):
                            mdContent += int(child.name[1])*"#"
                            mdContent += " "
                            mdContent += child.contents[0].text
                            mdContent += " " + int(child.name[1])*"#" + "\n\n"
                        if child.name == "ol":
                            for line in child.contents:
                                if line.name == "li":
                                    mdContent += "1. "
                                    mdContent += line.text
                                    mdContent += "\n"
                        if child.name == "ul":
                            for line in child.contents:
                                if line.name == "li":
                                    mdContent += "* "
                                    mdContent += line.text
                                    mdContent += "\n"
                        if child.name == "pre":
                            mdContent += "```\n"
                            mdContent += child.get_text()
                            mdContent += "\n```\n\n"
                        if child.name == "table":
                            mdContent += str(child)
                            mdContent += "\n\n"

            browser.get(bookFQDN + '/books/lab/page/create')

            title = browser.find_element_by_id('name')
            title.clear()
            title.send_keys(bullet['title'])
            print(bullet['title'])
            draft = browser.find_element_by_tag_name('textarea')
            # loop through string to show progress of send_keys
            for i in mdContent:
                draft.send_keys(i)
            browser.find_element_by_id('save-button').click()
            time.sleep(2)

browser.close()
